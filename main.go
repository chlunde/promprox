package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"fmt"
	"io"
	"io/ioutil"

	"crypto/tls"
	"flag"
	"log"
	"math/big"
	"net/http"
	"strconv"
	"time"
)

var (
	certFile = flag.String("cert", "", "PEM encoded certificate file for the proxy")
	keyFile  = flag.String("key", "", "PEM encoded private key file for the proxy")
	portMin  = flag.Int("port-min", 9000, "minimum port to proxy to")
	portMax  = flag.Int("port-max", 9500, "maximum port to proxy to")
	addr     = flag.String("addr", ":8999", "listen port")
	caFile   = flag.String("CA", "", "PEM encoded CA for authenticating scrapers")

	validNames stringslice
)

// fallbackKeypair generates a random self-signed certificate which
// may be used as a fallback for setups where it is not
// possible/viableto provision a certificate for each proxy
func fallbackKeypair() (*tls.Certificate, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(time.Now().UnixNano()),
		Subject: pkix.Name{
			Organization: []string{"Acme Co"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(10 * 365 * 24 * time.Hour),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		DNSNames:              []string{"localhost.localdomain"},
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, err
	}

	return &tls.Certificate{
		Certificate: [][]byte{derBytes},
		PrivateKey:  priv,
	}, nil
}

func main() {
	flag.Var(&validNames, "scraper-name", "Add `fqdn` as a valid scraper name for TLS authentication")
	flag.Parse()

	var certPool *x509.CertPool
	if *caFile != "" {
		pemCerts, err := ioutil.ReadFile(*caFile)
		if err != nil {
			log.Fatalf("failed loading trusted CA certificates from %v: %v\n", *caFile, err)
		}

		certPool = x509.NewCertPool()
		if !certPool.AppendCertsFromPEM(pemCerts) {
			log.Fatalf("failed loading certificates from %v", *caFile)
		}
	}

	// from https://blog.cloudflare.com/exposing-go-on-the-internet/
	tlsConfig := &tls.Config{
		// Causes servers to use Go's default ciphersuite preferences,
		// which are tuned to avoid attacks. Does nothing on clients.
		PreferServerCipherSuites: true,
		// Only use curves which have assembly implementations
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519,
		},
		ClientAuth: tls.RequireAnyClientCert,
		ClientCAs:  certPool,

		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}

	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/metrics/", func(w http.ResponseWriter, req *http.Request) {
		valid := false

		// TODO: Move to middleware?  Make this validator option.  The
		// difference is that we do not validate key usage, only CA
		// and DNS name
		opts := x509.VerifyOptions{
			Roots:         tlsConfig.ClientCAs,
			Intermediates: x509.NewCertPool(),
			//KeyUsages:     []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth},
		}

		for _, cert := range req.TLS.PeerCertificates[1:] {
			opts.Intermediates.AddCert(cert)
		}

		_, err := req.TLS.PeerCertificates[0].Verify(opts)
		if err != nil {
			log.Println("tls: failed to verify client's certificate: " + err.Error())
			http.Error(w, "Invalid client certificate", http.StatusUnauthorized)
			return
		}

	outer:
		for _, cert := range req.TLS.PeerCertificates {
			for _, name := range cert.DNSNames {
				for _, validName := range validNames {
					if validName == name {
						valid = true
						break outer
					}
				}
			}
		}

		log.Println(req.TLS.PeerCertificates[0].DNSNames)

		if !valid {
			log.Printf("%v: peer certificates %v does not match %v", req.RemoteAddr, req.TLS.PeerCertificates[0].DNSNames, validNames)
			http.Error(w, "DNSNames does not match", http.StatusUnauthorized)
			return
		}

		port, err := strconv.Atoi(req.FormValue("port"))
		if err != nil || port < *portMin || port > *portMax || req.Method != "GET" {
			http.Error(w, "Bad request", http.StatusForbidden)
			return
		}

		if req.Header.Get("X-Client") == "promprox" {
			http.Error(w, "Loop", http.StatusLoopDetected)
			return
		}

		// TODO: Timeouts?
		// more headers, (Accept, etc.)?
		backend := fmt.Sprintf("http://127.0.0.1:%d/metrics", port)
		childReq, err := http.NewRequest("GET", backend, nil)
		if err != nil {
			log.Printf("Client error: %v: %v\n", backend, err)
			http.Error(w, "Bad gateway: "+err.Error(), http.StatusBadGateway)
			return
		}

		if req.Header.Get("Accept") != "" {
			childReq.Header.Set("Accept", req.Header.Get("Accept"))
		}

		resp, err := http.DefaultClient.Do(childReq)
		if err != nil {
			log.Printf("Client error: %v: %v\n", backend, err)
			http.Error(w, "Bad gateway: "+err.Error(), http.StatusBadGateway)
			return
		}

		w.Header().Set("Content-Type", resp.Header.Get("Content-Type"))
		w.Header().Set("X-Client", "promprox")
		w.WriteHeader(resp.StatusCode)
		io.Copy(w, resp.Body)
		resp.Body.Close()
	})

	serveMux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		http.Error(w, "Not found", http.StatusNotFound)
	})

	srv := &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		TLSConfig:    tlsConfig,
		Handler:      serveMux,
		Addr:         *addr,
	}

	// If no certificate is provided, generate a selfsigned
	// certificate on the fly for testing, or environments where the
	// prometheus scraper does not need to verify the prometheus
	// proxy/servers
	if *certFile == "" {
		cert, err := fallbackKeypair()
		if err != nil {
			log.Fatalf("Could not create self signed certificate: %v\n", err)
		}
		tlsConfig.Certificates = []tls.Certificate{*cert}
	}

	log.Fatal(srv.ListenAndServeTLS(*certFile, *keyFile))
}

// stringslice implements flag.Value.  Each time the flag is given,
// the value is appended to the slice
type stringslice []string

func (s *stringslice) String() string {
	return fmt.Sprintf("%v", *s)
}

func (s *stringslice) Set(value string) error {
	*s = append(*s, value)

	return nil
}
